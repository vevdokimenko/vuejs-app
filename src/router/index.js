import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/webp",
      name: "webp",
      component: () => import("../views/Page.vue"),
      props: {
        title: "Webp template",
        src: "img/1.sm.webp",
        alt: "webp img",
        text: "This is a webp image",
      },
    },
    {
      path: "/svgz",
      name: "svgz",
      component: () => import("../views/Page.vue"),
      props: {
        title: "Svgz example",
        src: "img/9656938.fig.003.svgz",
        alt: "svgz img",
        text: "This is a svgz image",
      },
    },
  ],
});

export default router;
